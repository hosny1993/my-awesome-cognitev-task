/**
 * @description: main application file
 */

require('./config/config');

const express = require('express');
const bodyParser = require('body-parser');

const { logToServerLogs } = require('./utilites/log');
const { loadUserRoutes } = require('./routes/user.routes');

const app = express();
const port = process.env.PORT;
process.env.SERVERLOGSDIR = __dirname;

app.use(bodyParser.json());

// Add middleware to log all requests
app.use((req, res, next) => {
    let log = `${req.method} ${req.url}`;

    logToServerLogs(log);
    next();
});

// Add routes to express application
loadUserRoutes.call(undefined, app, (log) => {
    logToServerLogs(log);
});

// Start express application
app.listen(port, () => {
    let str = `Server is connected to ${port}`;
    logToServerLogs(str);
});

module.exports = { app };
