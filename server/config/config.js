/**
 * @description: Change mongodb database according to enviroment mode
 */

let env = process.env.NODE_ENV || 'development';

if (env === 'development') {
    process.env.PORT = 3000;
    process.env.MONGODB_URI = 'mongodb://localhost:27017/UsersApp';
} else if (env === 'test') {
    process.env.PORT = 3000;
    process.env.MONGODB_URI = 'mongodb://localhost:27017/UsersAppTest';
} else if (evn === 'production') {
    process.env.MONGODB_URI = '';
}
