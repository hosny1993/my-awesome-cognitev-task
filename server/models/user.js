/**
 * @description: User model object
 */

const mongoose = require('mongoose');
const validator = require('validator');

let UserSchema = new mongoose.Schema({
    firstName: {
        type: String, default: 'blank', required: true
    },
    lastName: {
        type: String, default: 'blank', required: true
    },
    countryCode: {
        type: String, default: 'blank', required: true
    },
    gender: {
        type: String, default: 'blank', required: true
    },
    phoneNumber: {
        type: String, default: 'blank', required: true, unique: true, validate: (val) => {
            return /^\+?[1-9]\d{1,14}$/.test(val); // E.164
        }
    },
    birthDate: {
        type: String, default: 'blank', required: true, validate: (val) => {
            return /([12]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01]))/.test(val.toLowerCase()); // yyyy-mm-dd
        }
    },
    email: {
        type: String, required: false, trim: true,
        validate: (val) => {
            return validator.isEmail(val);
        }
    },
    avatar: {
        type: String, default: 'blank', required: true, validate: (val) => {
            return !validator.isEmpty(val);
        }
    }
});

let User = mongoose.model('User', UserSchema);
module.exports = { User, UserSchema };
