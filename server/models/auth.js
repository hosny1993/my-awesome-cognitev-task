/**
 * @description: Authentication model object
 */

const mongoose = require('mongoose');
const validator = require('validator');
const jwt = require('jsonwebtoken');

// salting the encrypted value
const secert = 'abc123@secert';

let AuthSchema = new mongoose.Schema({
    phoneNumber: {
        type: String, default: 'blank', required: true, unique: true, validate: (val) => {
            return /^\+?[1-9]\d{1,14}$/.test(val); // E.164
        }
    },
    password: {
        type: String, required: true, validate: (val) => {
            let minMaxLength = /^[\s\S]{8,32}$/,
                upper = /[A-Z]/,
                lower = /[a-z]/,
                number = /[0-9]/,
                special = /[ !"#$%&'()*+,\-./:;<=>?@[\\\]^_`{|}~]/;

            if (minMaxLength.test(val) && upper.test(val) &&
                lower.test(val) && number.test(val) &&
                special.test(val)) {
                return true;
            }

            return false;
        }
    },
    tokens: [{
        access: {
            type: String,
            required: true
        },
        token: {
            type: String,
            required: true
        }
    }]
});

// generate token according to user phone number
AuthSchema.methods.generateAuthToken = function () {
    let authModel = this;
    let access = 'auth';

    let token = jwt.sign({
        _phone: authModel.phoneNumber, _password: authModel.password, access
    }, secert);

    authModel.tokens.push({ access, token });

    return authModel.save().then(() => {
        return token;
    });
};

// find authentication model by token
AuthSchema.statics.findByToken = function (token) {
    let authModel = this;
    let decoded = null;
    
    try {
        decoded = jwt.verify(token, secert);
    } catch (e) {
        return Promise.reject();
    }
    
    return AuthModel.findOne({
        phoneNumber: decoded._phone,
        password: decoded._password
    });
}

// find authentication model by token
AuthSchema.statics.findByPhone = function (phoneNumber) {
    return AuthModel.findOne({
        phoneNumber: phoneNumber,
    });
}

let AuthModel = mongoose.model('AuthModel', AuthSchema);
module.exports = { AuthModel };