/**
 * @description: Configure application routes
 */

const _ = require('lodash');
const multer = require('multer');

const { mongoose } = require('../db/mongoose');
const { User } = require('../models/user');
const { AuthModel } = require('../models/auth');

let upload = multer({ dest: 'avatars/' })

let fieldsArr = [
    '_id', 'firstName', 'lastname', 'countryCode', 'phoneNumber', 'geneder', 'birthDate', 'email'
];

let loadUserRoutes = (app, saveLogs) => {
    app.post('/user', upload.single('avatar'), (req, res) => {
        User.findOne({ phoneNumber: req.body.phoneNumber }).then((user) => {
            if (!user) {
                let body = _.pick(req.body, fieldsArr);
                let myUser = new User(body);

                if (req.file.mimetype === 'image/png' || 
                    req.file.mimetype === 'image/jpeg' || 
                    req.file.mimetype === 'image/jgp') {
                        myUser.avatar = req.file.destination + req.file.filename;
                }

                myUser.save().then((doc) => {
                    res.status(201);
                    res.send(_.pick(doc, fieldsArr));
                }, (err) => {
                    res.status(500);
                    res.send(JSON.stringify(new User()));
                    saveLogs('Unable to save new user', err);
                });
            } else {
                res.status(208); // It's already created
                res.send(_.pick(user, fieldsArr));
            }
        });
    });

    app.post('/user/auth', (req, res) => {
        let body = _.pick(req.body, ['phoneNumber', 'password']);
        let authModel = new AuthModel(body);

        authModel.save().then(() => {
            return authModel.generateAuthToken();
        }).then((token) => {
            res.header('x-auth', token).send({
                status: 'Success'
            });
        }).catch((err) => {
            saveLogs(`Unable to generate token ${err}`);

            AuthModel.findByPhone(body.phoneNumber).then((authModel) => {
                if (authModel.tokens[0]) {
                    res.header('x-auth', authModel.tokens[0].token).send({
                        status: 'Success'
                    });
                } else {
                    res.status(400).send({
                        status: 'Failed'
                    });
                }
            });
        });
    });

    app.post('/user/verfiy', upload.single('avatar'), (req, res) => {
        let body = _.pick(req.body, ['phoneNumber', 'status']);
        let token = req.get('x-auth');

        User.findOne({ phoneNumber: body.phoneNumber }).then((user) => {
            if (user) {
                AuthModel.findByToken(token).then((authModel) => {
                    if (authModel) {
                        res.status(200).send({
                            status: 'Success',
                            user: _.pick(user, fieldsArr)
                        });
                    } else {
                        res.status(401).send({
                            status: 'Failed'
                        });
                    }
                }).catch((e) => {
                    saveLogs(`Unable to generate token ${e}`);
                    res.status(401).send({
                        status: 'Failed'
                    });
                });
            } else {
                res.status(400).send({
                    status: 'Failed'
                });
            }
        });
    });
}

module.exports = { loadUserRoutes };