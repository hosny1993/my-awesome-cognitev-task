/**
 * @description: Test all tasks
 */

const request = require('supertest');

const app = require('../app').app;

const { User } = require('../models/user');
const { AuthModel } = require('../models/auth');

// clean all data in userapptest db
beforeAll(() => {
    User.remove({}).then();
    AuthModel.remove({}).then();
});

// Task 1
describe('POST /user, New User', () => {
    test('respond with json', (done) => {
        console.log(__dirname + '../' + '../')
        request(app)
            .post('/user')
            .set('Accept', 'application/json')
            .field({
                "firstName": "Mohamed",
                "lastname": "Hosny",
                "countryCode": "EG",
                "phoneNumber": "+2011584268481",
                "geneder": "male",
                "birthDate": "1993-04-20",
                "email": "hosny1993.bio@gmail.com"
            })
            .attach('avatar', __dirname + '/../../' + 'playground/icon.png')            
            .expect(201)
            .expect('Content-Type', /json/)
            .end(done);
    });
});

// Task 1
describe('POST /user, Already Created User', () => {
    test('respond with json', (done) => {
        request(app)
            .post('/user')
            .set('Accept', 'application/json')
            .field({
                "firstName": "Mohamed",
                "lastname": "Hosny",
                "countryCode": "EG",
                "phoneNumber": "+2011584268481",
                "geneder": "male",
                "birthDate": "1993-04-20",
                "email": "hosny1993.bio@gmail.com",
            })
            .attach('avatar', __dirname + '/../../' + 'playground/icon.png')            
            .expect(208)
            .expect('Content-Type', /json/)
            .end(done);
    });
});

// Task 2
describe('POST /user/auth, Get Auth Token', () => {
    test('should retun auth token', (done) => {
        request(app)
            .post('/user/auth')
            .set('Accept', 'application/json')
            .send({
                "password": "h@92348192@H",
                "phoneNumber": "+201158426841"
            })
            .expect(200)
            .end((err, res) => {
                if (err) {
                    return done(err);
                }

                AuthModel.findByToken(res.get('x-auth')).then((authModel) => {
                    expect(authModel.tokens[0].token).toBeDefined();
                    return done();
                }).catch((e) => done(e));
            });
    });
});

// Task 3
describe('POST /user/verfiy, Verify User Creaditionals', () => {
    test('respond with json', (done) => {
        request(app)
            .post('/user/verfiy')
            .set('Accept', 'application/json')
            .set('x-auth', `eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfcGhvbmUiOiIrMjAxMTU4NDI2ODQxIiwiX3Bhc3N3b3JkIjoiaEA5MjM0ODE5MkBIIiwiYWNjZXNzIjoiYXV0aCIsImlhdCI6MTUyNjM5NTUwMn0.tpSRovGwQ-t4cxBPOrRw9wIc1kjP59e0dZqtbFdt1SQ`)
            .field({
                "firstName": "Mohamed",
                "lastname": "Hosny",
                "countryCode": "EG",
                "phoneNumber": "+2011584268481",
                "geneder": "male",
                "birthDate": "1993-04-20",
                "email": "hosny1993.bio@gmail.com"
            })
            .attach('avatar', __dirname + '/../../' + 'playground/icon.png')            
            .expect(200)
            .expect('Content-Type', /json/)
            .end(done);
    });
}); 