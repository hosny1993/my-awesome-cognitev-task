const fs = require('fs');

let logToServerLogs = (log) => {
    fs.appendFile(process.env.SERVERLOGSDIR + '/server.log',
        `${new Date().toString()}: ${log}` + '\n');
}

module.exports = { logToServerLogs };